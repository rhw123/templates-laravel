<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class castModel extends Model
{
    protected $table = "cast";
    protected $fillable = ['nama', 'umur', 'bio'];
}
