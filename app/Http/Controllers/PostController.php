<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\castModel;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = castModel::all();
        return view('cast.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio'  => 'required'
    	]);
 
        castModel::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'bio'  => $request->bio
    	]);
 
    	return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = castModel::find($id);
        return view('cast.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = castModel::find($id);
        return view('cast.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'bio'
        ]);

        $post = castModel::find($id);
        $post->nama = $request->nama;
        $post->umur = $request->umur;
        $post->bio = $request->bio;
        $post->update();
        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = castModel::find($id);
        $post->delete();
        return redirect('/post');
    }
}
