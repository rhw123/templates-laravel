<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index()
    {
        $post = DB::table('cast')->get();
        
         return view('index', compact('post'));
    }

    public function create()
    {
       return view('createCast');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"  => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $post = DB::table('cast')->where('id', $id)->first();
        return view('show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('cast')->where('id', $id)->first();
        return view('edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required:cast',
            'umur' => 'required',
            'bio'  => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio'  => $request['bio']
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
