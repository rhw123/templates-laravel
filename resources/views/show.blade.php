@extends('master')

@section('judul')
    Show cast
@endsection


@section('title')

    <h4 class = "pl-2 pt-3 pb-3">Cast ID ke  = {{ $post->id }}</h4>

@endsection

@section("content")

    <table class = "table table-bordered table">
        <thead class =  "thead-light">
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
        </thead>
        <tbody>
            <td>{{ $post->nama }}</td>
            <td>{{ $post->umur }}</td>
            <td>{{ $post->bio }}</td>
        </tbody>
    </table>
    <a href="/cast" class = "btn btn-primary btn-sm">Kembali</a>

@endsection