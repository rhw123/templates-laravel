@extends('master')


@section('judul')
    halaman edit data
@endsection


@section('title')
  <h4 class = "pl-2 pt-3 pb-3">Edit Data</h4>
@endsection

@section('content')

<form action="/post/{{ $post->id }}" method="POST">
   @csrf
   @method('put')
        <div class= "container">
            <div class = "row">
                <div class = "col-md-8">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" name= "nama" value = "{{ $post->nama }}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                     @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Umur</label>
                    <input type="text" name = "umur" value = "{{ $post->umur }}" class="form-control" id="exampleInputPassword1">
                    @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Bio</label>
                    <input type="text" name = "bio" value = "{{ $post->bio }}" class="form-control" id="exampleInputPassword1">
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary btn-sm">Ubah</button>
                <a href="/cast" class = "btn btn-danger btn-sm">Kembali</a>
                </div>
            </div>
        </div>
   </form>

@endsection