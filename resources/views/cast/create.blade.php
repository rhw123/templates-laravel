@extends('master')

@section('judul')
    create data
@endsection


@section("title")

  <h4 class = "pl-3 pt-5 pb-5">Tambah Data Cast</h4>

@endsection

@section('content')

   <form action="/post" method="POST">
   @csrf
        <div class= "container">
            <div class = "row">
                <div class = "col-md-8">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" name= "nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                     @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Umur</label>
                    <input type="text" name = "umur" class="form-control" id="exampleInputPassword1">
                    @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Bio</label>
                    <input type="text" name = "bio" class="form-control" id="exampleInputPassword1">
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary btn-sm">simpan</button>
                </div>
            </div>
        </div>
   </form>


@endsection