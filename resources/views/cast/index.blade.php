@extends("master")


@section('judul')

Halaman utama

@endsection


@section('title')

    <h4 class = "pl-2 pt-3 pb-3">halaman cast</h4>

    <div class = "container">
        <div class = "row">
            <div class = col-md-12>
            <a href="/post/create" class = "btn btn-primary btn-sm mb-3">Tambah Data</a>
            <table class = "table table-bordered table">
                <thead class = "thead-light">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>umur</th>
                        <th>bio</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($model as $p => $value)
                    <tr>
                        <td>{{$p + 1}}</td>
                        <td>{{ $value->nama }}</td>
                        <td>{{ $value->umur}}</td>
                        <td>{{ $value->bio }}</td>
                        <td>{{ $value->created_at }}</td>
                        <td>{{ $value->updated_at }}</td>
                        <td>
                            <form action="/cast/{{$value->id}}" method = "POST">
                                <a href="/cast/show/{{ $value->id }}" class = "btn btn-success btn-sm">Show</a>
                                <a href="/cast/{{ $value->id }}" class = "btn btn-danger btn-sm">edit</a>
                                @csrf
                                @method("DELETE")
                                <input type="submit" class = "btn btn-warning btn-sm" value = "delete">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>

@endsection